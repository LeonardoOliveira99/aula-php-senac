<?php

class Usuario{

    public function __construct(){

        echo "Iniciando conexão com o <strong>SGDB<strong>";

    }
    
    private $id;
    private $email;
    private $senha;

    //Setters
    public function setId(int $id){

        $this->id = $id;

    }

    public function setEmail(string $email){

        $this->email = $email;

    }

    public function setSenha(string $senha){

        $this->senha = $senha;

    }

    // Getters
    public function getId(int $id) : int{

        return $this->id;

    }

    public function getEmail(string $email) : string{

        return $this->email;

    }

    public function getSenha(string $senha) : string{

        return $this->senha;

    }

    public function __destruct(){
        echo "<br>Fechando a conexão com o <strong>SGDB<strong>";
    }
}