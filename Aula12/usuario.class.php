<?php

class Usuario{

    public function __construct(){

        $this->objDb = new mysqli('localhost', 'root', '', 'aula_php', 3307);

    }

    public function saveUsuario(){

        $objStmt = $this->objDb->prepare('
        REPLACE INTO tb_usuario (id, nome, email, senha) VALUES (?,?,?,?)' );

        $objStmt->bind_param('isss', 
        $this->id, $this->nome, $this->email, $this->senha);

        if($objStmt->execute() ){
            return true;
        }else{
            return false;
        }
    }
    public function listaUsu(){

        $objStmt = $this->objDb->query('SELECT * from tb_usuario');
        


        // $objStmt->bind_param('isss', 
        // $this->id, $this->nome, $this->email, $this->senha);

        return $objStmt;
    }

    //     $preparada = mysqli_prepare($db, 'INSERT INTO tb_usuario (nome, email) VALUES (?,?)' );
    //     mysqli_stmt_bind_param($preparada, "is", $email, $senha);
    //     mysqli_stmt_execute($preparada);
    //     if(mysqli_stmt_execute($preparada)){
    //         echo "Deu certo!!";
    //     };

    // }
    private $id;
    private $nome;
    private $email;
    private $senha;
    private $objdb;

    //Setters
    public function setId(int $id){

        $this->id = $id;

    }

    public function setNome(string $nome){

        $this->nome = $nome;

    }
    public function setEmail(string $email){

        $this->email = $email;

    }

    public function setSenha(string $senha){

        // $this->senha = $senha;
        {
            $this-> senha = $hash = password_hash( $this->senha, PASSWORD_DEFAULT);
        }

    }

    // Getters
    public function getId(int $id) : int{

        return $this->id;

    }
    public function getNome(int $nome) : int{

        return $this->nome;

    }

    public function getEmail(string $email) : string{

        return $this->email;

    }

    public function getSenha(string $senha) : string{

        return $this->senha;

    }

    public function __destruct(){
        echo "<br>Fechando a conexão com o <strong>SGDB<strong>";
        unset($this->objDb);
    }
}