<html lang="en">
  <head>
 
   
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title></title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>

   
    <div class="container-form" id="form">
      
    <form>
        
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Nome</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="nome" alt="Nome">
          </div>
        </div>
        <div class="form-group row">
          <label for="inputPassword3" class="col-sm-2 col-form-label">Endereço</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="endereco" alt="Endereço">
          </div>
        </div>
        <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">CEP</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="cep" alt="CEP"><span id="cep-val"></span>
                </div>
        </div>
        <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="email" alt="E-mail">
                </div>
        </div>
        <span id="span"></span>
       
        <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Enviar</button>
          </div>
        </div>
      </form>
    </div>

    <script>

        var enviar = document.getElementById('form');
        var email = document.getElementById('nome');
        var email = document.getElementById('endereco');
        var email = document.getElementById('cep');
        var email = document.getElementById('email');
        var span = document.getElementById('span');
        var verifica = document.querySelectorAll('.form-control');


        enviar.onsubmit = function(event){
            var msg = "Preencha todos os campos! ("
            var validado = true;
             for(var i=0; i < verifica.length;i++){
                 
                 if(verifica[i].value == ""){
                     validado = false;
                     msg = msg + verifica[i].alt + ",";
                     var id = verifica[i].id;
                     id = id + "-val";
                     document.getElementById (id).innerHTML = "preencha o " + verifica[i].alt;
                 }
                    

             };
             if (validado == false) {
                 msg = msg.substring (0,msg.length-1);
                 msg = msg + ')';
                document.getElementById('span').innerHTML = msg;
                }

             event.preventDefault();
             
             }
    
        
        
    </script>
    
  </body>
</html>