<?php
echo "<pre>";
//Constante no PHP

define('QTD_PAGINAS',10);
$valor = 15452;

echo "Valor da minha constante é: " . QTD_PAGINAS . " e a Variável é: ". $valor;

//Variável para passar valor para uma constante

$ip_do_banco = '192.168.45.12';

define('IP_DO_BANCO', $ip_do_banco);

echo "\no IP do banco é :" . $ip_do_banco;

//Constantes Mágicas
echo "\nEstou na linha: " . __LINE__;
echo "\nEstou na linha: " . __LINE__;
echo "\nEste é o arquivo" . __FILE__;

//Muito om para depurar o código
echo"\n\n";
var_dump($ip_do_banco);

//HORA DO VETOR!!! ARRAY
$dias_da_semana = ['dom','seg','ter','qua','qui','sex','sab'];

$dias_da_semana[0] = 'dom';//outra forma de associar à um vetor(array)


unset($dias_da_semana);//destrói variável UNSET

$dias_da_semana = array(0 => 'dom',1 => 'seg',2 => 'ter',3 => 'qua',4 => 'qui',5 => 'sex',6 => 'sab');

//$dias_da_semana
var_dump($dias_da_semana);











//Testando For
// $teste_variavel = 0;


//  for($i=0; $i<20; $i++){
//      $teste_variavel++;
//      echo "\n$teste_variavel";

//  };
//  //Testando if
//  if($i < 15){
//      $teste_variavel = 1;
//      echo "\n$teste_variavel";
//  }else{
//      $teste_variavel = 777;
//      echo "\n$teste_variavel";
//  }



echo "</pre>";